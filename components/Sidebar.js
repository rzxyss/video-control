import Link from "next/link"
import { useState } from "react"
import {
    MdDashboard,
    MdOutlineDevices,
    MdPermMedia,
    MdQueue
} from 'react-icons/md'
import {
    GrAdd,
    GrEdit
} from 'react-icons/gr'
import {
    FaChevronDown,
    FaChevronUp
} from 'react-icons/fa'

export default function Sidebar(){
    const [media, openMedia] = useState('')
    const [queue, openQueue] = useState('')

    return(
        <div className='flex flex-col w-4/12 lg:w-2/12 h-screen text-gray-500'>
            <div className='flex p-5 justify-center'>
                <h1 className='font-semibold text-lg'>Video Control</h1>
            </div>
            <div className='border-b mb-5'></div>
            <div className='text-sm p-2'>Menu</div>
            <Link href='/'>
                <div className='flex flex-row items-center rounded-lg hover:bg-gray-500/20 cursor-pointer'>
                    <div className='flex flex-row items-center p-3 px-5'>
                        <MdDashboard />
                        <span className='font-bold pl-1'>Home</span>
                    </div>
                </div>
            </Link>
            <Link href='/platform'>
                <div className='flex flex-row items-center rounded-lg hover:bg-gray-500/20 cursor-pointer'>
                    <div className='flex flex-row items-center p-3 px-5'>
                        <MdOutlineDevices />
                        <span className='font-bold pl-1'>Platform</span>
                    </div>
                </div>
            </Link>
            <div className='flex flex-row items-center rounded-lg hover:bg-gray-500/20'>
                <div className="flex flex-col justify-between">
                    <div className="flex justify-between">
                        <Link href='/media'>
                            <div className='flex items-center rounded-lg cursor-pointer'>
                                <div className='flex items-center p-3 px-5'>
                                    <MdPermMedia />
                                    <span className='font-bold pl-1'>Media</span>
                                </div>
                            </div>
                        </Link>
                        <div className='flex items-center p-3 px-5'>
                            <button onClick={() => openMedia(!media)}>
                                <FaChevronDown className={`${media ? 'hidden' : 'block'}`}/>
                                <FaChevronUp className={`${!media ? 'hidden' : 'block'}`}/>
                            </button>
                        </div>
                    </div>
                    <div className={`${media ? 'flex' : 'hidden'} flex-col px-3`}>
                        <div class={`pb-4 md:pb-0 md:justify-end`}>
                            <Link href='/media/addMedia'>
                                <div className='flex flex-row items-center rounded-lg cursor-pointer'>
                                    <div className='flex flex-row items-center p-3 px-5'>
                                        <GrAdd />
                                        <span className='font-bold pl-1'>Add Media</span>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className='flex flex-row items-center rounded-lg hover:bg-gray-500/20'>
                <div className="flex flex-col justify-between">
                    <div className="flex justify-between">
                        <Link href='/queue'>
                            <div className='flex items-center rounded-lg cursor-pointer'>
                                <div className='flex items-center p-3 px-5'>
                                    <MdQueue />
                                    <span className='font-bold pl-1'>Add Queue</span>
                                </div>
                            </div>
                        </Link>
                        <div className='flex items-center p-3 px-5'>
                            <button onClick={() => openQueue(!queue)}>
                                <FaChevronDown className={`${queue ? 'hidden' : 'block'}`}/>
                                <FaChevronUp className={`${!queue ? 'hidden' : 'block'}`}/>
                            </button>
                        </div>
                    </div>
                    <div className={`${queue ? 'flex' : 'hidden'} flex-col px-3`}>
                        <div class={`pb-4 md:pb-0 md:justify-end`}>
                            <Link href='/queue/addQueue'>
                                <div className='flex flex-row items-center rounded-lg cursor-pointer'>
                                    <div className='flex flex-row items-center p-3 px-5'>
                                        <GrAdd />
                                        <span className='font-bold pl-1'>Add Queue</span>
                                    </div>
                                </div>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}