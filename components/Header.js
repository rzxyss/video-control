import { CgProfile } from 'react-icons/cg'
import { IoMdSettings } from 'react-icons/io'

export default function Header(){
    return(
        <div className="flex shadow-sm bg-gray-50 p-4 justify-between items-center">
            <p className="text-gray-400">Hello, User</p>
            <div className='flex flex-row items-center'>
                <IoMdSettings className='w-7 h-7 mr-2 text-black/50 cursor-pointer' title='Setting' />
                <CgProfile className='w-8 h-8 cursor-pointer' title='Profile'/>
            </div>
        </div>
    );
}