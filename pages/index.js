import Header from "../components/Header"
import Sidebar from "../components/Sidebar"

export default function Home() {
    return (
        <div className="flex">
            <Sidebar />
            <div className="w-full"> 
                <Header />
                <div className="p-3">
                    <h1 className="text-2xl font-bold">Selamat Datang Di Video Control</h1>
                </div>
            </div>
        </div>
    )
}
