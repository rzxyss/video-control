import Sidebar from "../../components/Sidebar"
import Header from "../../components/Header"

export default function addMedia(){
    return(
        <div className="flex">
            <Sidebar />
            <div className="w-full"> 
                <Header />
                <div className="p-3">
                    <h1 className="text-2xl font-bold">Add Media</h1>
                    <form className="mt-8 space-y-6">
                        <div className="-space-y-px rounded-md shadow-sm">
                            <div>
                                <label className="sr-only">Name</label>
                                <input type="text" className="relative block w-full appearance-none rounded-none rounded-t-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm" placeholder="Input Name Media" />
                            </div>
                            <div>
                                <label className="sr-only">Url</label>
                                <input type="text" className="relative block w-full appearance-none rounded-none border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm" placeholder="Input URL" />
                            </div>
                            <div>
                                <label className="sr-only">Duration</label>
                                <input type="text" className="relative block w-full appearance-none rounded-none rounded-b-md border border-gray-300 px-3 py-2 text-gray-900 placeholder-gray-500 focus:z-10 focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm" placeholder="Input Duration" />
                            </div>
                        </div>
                        <div>
                            <button className="group relative flex w-full justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}